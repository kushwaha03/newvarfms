//
//  loginVC.swift
//  newVarFms
//
//  Created by karmaa lab on 26/04/19.
//  Copyright © 2019 kLab. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class loginVC: UIViewController {

    @IBOutlet var uNameTxt: UITextField!
    @IBOutlet var uPassTxt: UITextField!
    @IBOutlet var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()


        
        uNameTxt.layer.cornerRadius = 10
        uPassTxt.layer.cornerRadius = 10
        loginBtn.layer.cornerRadius = 10
        uNameTxt.layer.borderWidth  = 1
        uNameTxt.layer.borderColor  = UIColor.green.cgColor
        uPassTxt.layer.borderWidth  = 1
        uPassTxt.layer.borderColor  = UIColor.green.cgColor

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func doLogin(_ sender: Any) {

        let url = Config.api_base + "api-token-auth/"
//         let url: String = "https://varfms.karmaalab.net/api-token-auth/"
            Alamofire.request(url, method: .post,  parameters: ["username": self.uNameTxt.text!, "password": self.uPassTxt.text!])
//        Alamofire.request(url, method: .post,  parameters: ["username": self.uNameTxt.text!, "password": self.uPassTxt.text!])
            
            .responseJSON { (response) in
            if response.result.isSuccess {
                let json: JSON = JSON(response.result.value!)
                print(json)
                if let token = json["token"].string {
                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "homeVC") as? homeVC
                    vc?.token = token
                    
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            } else {
                print("error")
            }
        }


//        Alamofire.request(Config.url("/user/login/"),method: .get, parameters: ["email": self.uNameTxt.text!, "password": self.uPassTxt.text!])
//
//            .validate()
//            .responseJSON { response in

//                switch response.result {
//                case .success(let data):
//                    var jsonData = JSON(data)
//                    PineSimpleData.remove("preferences")
//                    self.alert(type: .success, heading:"WELL DONE!", text: "Login Successful!",confirmText: nil, dismissText: "AWESOME!", confirmTarget: nil, confirmAction: nil)
//
//
//                //self.setToken()
//                case .failure( _):
//                    let jsonData = JSON(data: response.data!)
//                    let message = jsonData["errors"].string
//                    self.alert(type: .error, heading:"OH SNAP!", text: message ?? "Looks Like Something Went Wrong", confirmText: nil, dismissText: "OK", confirmTarget: nil, confirmAction: nil)
//                }
//        }
    }
}
