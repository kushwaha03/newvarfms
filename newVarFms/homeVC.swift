//
//  homeVC.swift
//  newVarFms
//
//  Created by karmaa lab on 26/04/19.
//  Copyright © 2019 kLab. All rights reserved.
//

import UIKit
import  Alamofire
import  SwiftyJSON

class homeVC: UIViewController {
    @IBOutlet var qrCodeBtn: UIButton!
    
    @IBOutlet var menuBtn: UIButton!
    @IBOutlet var myAssetBtn: UIButton!
    @IBOutlet var leftView: UIView!
    var token:String?
     var array_Rnew = [[String : Any]]()
    var array_Rnew2 = [String ]()
    var arrayNew = [Any]()
    var hideBtn = false
    var assetData: [AssetMode] = []

    
    
        override func viewDidLoad() {
        super.viewDidLoad()

            if Device.DeviceType.IS_IPHONE_5_OR_LESS_6 {
//                print("IS_IPHONE_6")
                qrCodeBtn.widthAnchor.constraint(equalToConstant: 155.0).isActive = true
                qrCodeBtn.heightAnchor.constraint(equalToConstant: 160.0).isActive = true
                myAssetBtn.widthAnchor.constraint(equalToConstant: 155.0).isActive = true
                myAssetBtn.heightAnchor.constraint(equalToConstant: 160.0).isActive = true
            }
        // Do any additional setup after loading the view.
        leftView.isHidden = true
        qrCodeBtn.layer.borderWidth = 2
        qrCodeBtn.layer.borderColor = UIColor.black.cgColor
        myAssetBtn.layer.borderWidth = 2
        myAssetBtn.layer.borderColor = UIColor.black.cgColor
        qrCodeBtn.layer.cornerRadius = 10
        myAssetBtn.layer.cornerRadius = 10

    }
    
    @IBAction func assetsBtnAction(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "assetsVC") as? assetsVC
        vc?.token = token
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func hidenLeftVbtn(_ sender: Any) {
        if hideBtn {
        leftView.isHidden = true
        print("press fo hiden")
        }
    }
    @IBAction func menuActionBtn(_ sender: Any) {
         leftView.isHidden = false
         hideBtn = true
        
    }
    @IBAction func aboutBtn(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "aboutVC") as? aboutVC
   
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
