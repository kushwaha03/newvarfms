//
//  Config.swift
//  newVarFms
//
//  Created by karmaa lab on 30/04/19.
//  Copyright © 2019 kLab. All rights reserved.
//
import Alamofire
import SwiftyJSON
import Foundation
class Config {
    
    enum Mode {
        case live, test
    }
    static var app_mode = Mode.test
    
    static var api_base:String {
        switch Config.app_mode {
        case .live:
            return "https://varfms.karmaalab.net"
        case .test:
         
            return "http://192.168.1.5:8000/"
        }
    }
}
