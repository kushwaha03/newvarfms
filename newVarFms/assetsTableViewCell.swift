//
//  assetsTableViewCell.swift
//  newVarFms
//
//  Created by karmaa lab on 26/04/19.
//  Copyright © 2019 kLab. All rights reserved.
//

import UIKit

class assetsTableViewCell: UITableViewCell {
    @IBOutlet var cellBtn: UIButton!
    
    @IBOutlet var expandV: UIView!
    @IBOutlet var assetExNLbl: UILabel!
    @IBOutlet var expandArrowBtn: UIButton!
    @IBOutlet var assetNmLbl: UILabel!
    @IBOutlet var assetMLbl: UILabel!
    @IBOutlet var assetNLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
