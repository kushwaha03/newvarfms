//
//  assetsVC.swift
//  newVarFms
//
//  Created by karmaa lab on 26/04/19.
//  Copyright © 2019 kLab. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import  Alamofire
import  SwiftyJSON

class assetsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    @IBOutlet var locatBtn: UIButton!
    @IBOutlet var menuBtn: UIButton!
   
    @IBOutlet var leftView: UIView!
    @IBOutlet var headerV: UIView!
    @IBOutlet var mapV: UIView!
    @IBOutlet  var myMapView: GMSMapView!
    @IBOutlet var assetsBtn: UIButton!
    @IBOutlet var assetTbl: UITableView!
    var hideBtn = false

    let locationManager = CLLocationManager()
        var items = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]
    var items1 = ["A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1", "I1", "J1"]
    var items2 = ["A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2", "I2", "J2"]
    var expandedCells = [Int]()

 
    var assetData: [AssetMode] = []
    var token:String?
    var AssArr2 = [String:Any]()
    var AssArr : [[String:Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        assetTbl.rowHeight = 60
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        myMapView.isMyLocationEnabled = true
//        print(arrAssets!)
        
                    let headers: HTTPHeaders = [
                        "Authorization": "JWT " + token!
                    ]
//            let headers: HTTPHeaders = [
//                "Authorization": "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1NTkzNjg2MTgsImVtYWlsIjoibW9uYUB2YXJmbXMuY29tIiwidXNlcm5hbWUiOiJ2YXJzdXBlcmFkbWluIn0.SSEAIORrl0otHZ2I1Ej8h_ghY8fOT8ntR3s-SRjySIg"
//            ]
                    let url = Config.api_base + "assets/asset-list/"
//                    let url: String = "https://varfms.karmaalab.net/assets/asset-list/"
                    Alamofire.request(url, method: .get,  headers: headers).responseJSON { (response) in
                            if response.result.isSuccess {
                                let json: JSON = JSON(response.result.value!)
                                print(json)
                                if let responseData = response.result.value as? [[String: Any]]{
                                    self.AssArr=responseData
                                   self.assetTbl.reloadData()
                                }
//                               print(self.AssArr)

                            } else {
                                print("error")
                            }
                    }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.AssArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! assetsTableViewCell
//        cell.textLabel?.text = items[indexPath.row]
//        cell.textLabel?.text = indexPath.row
        cell.selectionStyle = .none
        cell.cellBtn.tag = indexPath.row
        cell.cellBtn.addTarget(self, action: #selector(self.tappedButton(sender:)), for: .touchUpInside)
        cell.expandArrowBtn.tag = indexPath.row
        cell.expandArrowBtn.addTarget(self, action: #selector(self.tappedButtonForExpand(sender:)), for: .touchUpInside)

        let object =  self.AssArr[indexPath.row]

        cell.assetNmLbl.text = object["assetnumber"] as? String
        cell.assetMLbl.text = object["name"] as? String
        if let manufacturer = object["manufacturer"]  as? String {
        cell.assetNLbl.text = manufacturer
        } else {
           cell.assetNLbl.text = "manufacturer"
        }
        cell.assetExNLbl.text = object["assetnumber"] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
     @objc func tappedButtonForExpand(sender : UIButton){
//        assetTbl.rowHeight = 200
        print(sender.tag)
        if expandedCells.contains(sender.tag) {
            expandedCells = expandedCells.filter({ $0 != sender.tag})
        }
            // Otherwise, add the button to the array
        else {
            expandedCells.append(sender.tag)
        }
        // Reload the tableView data anytime a button is pressed
        assetTbl.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if expandedCells.contains(indexPath.row) {
                return 180
            } else {
                return 70
        }
    }
    @objc func tappedButton(sender : UIButton){
        // Your code here
        print("button tag is :",sender.tag)
        let object =  self.AssArr[sender.tag]
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "assetsDetailsVC") as? assetsDetailsVC
        vc?.titleName = object["name"] as? String
        vc?.titleNumber = object["assetnumber"] as? String
         vc?.token = token
        if let manufacturer = object["manufacturer"]  as? String {
        print(manufacturer)
        } else {
           
        }
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!,
                                              longitude: (location?.coordinate.longitude)!,
                                              zoom: 15.0)
        myMapView.camera = camera
        myMapView.animate(to: camera)
    }
    
    @IBAction func goBack(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func assetClickAct(_ sender: Any) {
//    mapV.isHidden = true
        assetTbl.isHidden = false
                headerV.backgroundColor = #colorLiteral(red: 0.1252238876, green: 0.6752996151, blue: 0.9989272992, alpha: 1)
        
    }
    @IBAction func locatClickAct(_ sender: Any) {
        assetTbl.isHidden = true
        myMapView.isHidden = false
        headerV.backgroundColor = .red
        
//        let camera = GMSCameraPosition.camera(withLatitude: 12.9716, longitude: 77.5946, zoom: 15.0)
//        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//        myMapView = mapView
//
//        // Creates a marker in the center of the map.
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//        marker.title = "its Me"
//        marker.snippet = "Australia"
//        marker.map = mapView
    }
    @IBAction func menuActionBtn(_ sender: Any) {
        if !hideBtn {
        leftView.isHidden = false
        hideBtn = true
        } else {
        leftView.isHidden = true
            hideBtn = false
        }
        
    }
    @IBAction func hideLeftVBtnAct(_ sender: Any) {
//        if hideBtn {
//            leftView.isHidden = true
            print("press fo hiden")
//        }
    }
    @IBAction func aboutBtn(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "aboutVC") as? aboutVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
