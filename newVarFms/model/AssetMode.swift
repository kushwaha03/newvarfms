//
//  AssetMode.swift
//  newVarFms
//
//  Created by karmaa lab on 30/04/19.
//  Copyright © 2019 kLab. All rights reserved.
//

import Foundation
struct AssetMode {
//    var assetNm : String
//    var assetNo : String
   
    var name : String
    var manufacturer : String
    var serialnumber : String
    var assetnumber : String
    var status : Bool
    var description : String
    var qrimage : String
    var site : Int

    init(_ dictionary: [String: Any]) {
      self.name = dictionary["name"] as? String ?? ""
        self.manufacturer = dictionary["manufacturer"] as? String ?? ""
        self.serialnumber = dictionary["serialnumber"] as? String ?? ""
        self.assetnumber = dictionary["assetnumber"] as? String ?? ""
        self.status = dictionary["status"] as? Bool ?? false
        self.description = dictionary["description"] as? String ?? ""
        self.qrimage = dictionary["qrimage"] as? String ?? ""
        self.site = dictionary["site"] as? Int ?? 0
    }
}

