//
//  assetsDetailsVC.swift
//  newVarFms
//
//  Created by karmaa lab on 26/04/19.
//  Copyright © 2019 kLab. All rights reserved.
//

import UIKit
import Alamofire
import  SwiftyJSON
var scView:UIScrollView!
let buttonPadding:CGFloat = 0
var buttonPadding2:CGFloat = 10
//var xOffset:CGFloat = 10
var yOffset:CGFloat = 10
var factiveBtn = false
class assetsDetailsVC: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet var allTabAct: UIView!
    @IBOutlet var dataLbl: UILabel!
    @IBOutlet var assetNLbl: UILabel!
    var items = ["Aaaaaaaa", "Bbbbbbbb", "Ccccccccc", "Dddddddd", "Eeeeeee", "Fffffff", "Ghhhhhhh", "Hhhhhhhhh", "Iiiiiiiiii"]
     var titleName: String?
    var titleNumber: String?
     var token:String?
    var AssArrTab = [String]()
    var AssArrkey = [String]()
    var AssArrval = [Any]()
    var AssArrDe = [String:Any]()
    var AssArr : [[String:Any]] = []
    var AssFnm : [[String:Any]] = []
    var model = [AssetMode]()
     var activeTab = [Int]()
     var hideVBtn = false
     var image = UIImageView()
    var imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
         imagePicker.delegate = self
        var xOffset:CGFloat = 10
        
        let headers: HTTPHeaders = [
            "Authorization": "JWT " + token!
        ]
        
     
//                    let headers: HTTPHeaders = [
//                        "Authorization": "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ1c2VybmFtZSI6Imthcm1hYSIsImV4cCI6MTU1OTcyODMxOCwiZW1haWwiOiIifQ._ILZ1DiUYxtkJNUFSEUMoVu9hc9giibjYjvmyDuUhHM"
//                    ]
        let Furl: String = Config.api_base + "assets/asset-field-list/"
        Alamofire.request(Furl, method: .get, headers:headers)
            .responseJSON { (response) in
                if response.result.isSuccess {
                    let json: JSON = JSON(response.result.value!)
//                    print(json)
                    if let responseData = response.result.value as? [[String: Any]]{
//                        print(responseData)
                        self.AssFnm=responseData
                        
                    }
                } else {
                    print("error")
                }
        }
        
        let url: String = Config.api_base + "assets/all-detail-list-by-assetnumber/"
//
        Alamofire.request(url, method: .get, parameters:["assetnumber": titleNumber!], headers:headers)
//        Alamofire.request(url, method: .get, parameters:["assetnumber": "LA55 - 1"], headers:headers)
            .responseJSON { (response) in
            if response.result.isSuccess {
                let _: JSON = JSON(response.result.value!)
                  if let jsonD = response.result.value as? [String:Any]{
                    self.AssArrDe = jsonD
                    for(key,_) in jsonD {
                        self.AssArrTab.append(key)
                        
                    }
                    print(self.AssArrTab)
                    if let jsonArray = jsonD["asset"] as? [[String:Any]] {
                        for dic in jsonArray{
                            self.model.append(AssetMode(dic))
                            for(k,v) in dic {
                                print(k,v)
                                self.AssArrval.append(v)
                               self.AssArrkey.append(k)
                            }
                        }
                    }
                }

                scView = UIScrollView(frame: CGRect(x: 0, y: 200, width: self.view.bounds.width, height: 60))
                self.view.addSubview(scView)
                
                scView.backgroundColor = UIColor.blue
                scView.translatesAutoresizingMaskIntoConstraints = false
                
                let fbutton = UIButton()
                fbutton.tag = 0
                fbutton.backgroundColor = UIColor(red: 17.0/255.0, green: 119.0/255.0, blue: 151.0/255.0, alpha: 1.0)
                fbutton.setTitle("Asset", for: .normal)
                fbutton.backgroundColor = UIColor.blue
                fbutton.setTitleColor(.white, for: .normal)
                fbutton.frame = CGRect(x: xOffset, y: CGFloat(buttonPadding), width: 60, height: 60)
                xOffset = xOffset + CGFloat(buttonPadding) + fbutton.frame.size.width
                fbutton.addTarget(self, action: #selector(self.tappedAssetButton(sender:)), for: .touchUpInside)
                scView.addSubview(fbutton)
                
                for i in 0..<self.AssArrTab.count {
                    if !self.AssArrTab[i].contains("asset")  {
                    let abutton = UIButton()
                    abutton.tag = i
                    abutton.backgroundColor = UIColor(red: 17.0/255.0, green: 119.0/255.0, blue: 151.0/255.0, alpha: 1.0)
                        abutton.setTitle(self.AssArrTab[i], for: .normal)

                    
                    abutton.frame = CGRect(x: xOffset+10, y: CGFloat(buttonPadding), width: 120, height: 60)
                    xOffset = xOffset + CGFloat(buttonPadding) + abutton.frame.size.width
                    abutton.addTarget(self, action: #selector(self.tappedButton(sender:)), for: .touchUpInside)
                    scView.addSubview(abutton)
                    }
                }
                scView.contentSize = CGSize(width: xOffset, height: scView.frame.height)
            } else {
                print("error")
            }
        }
        
        
        dataLbl.text = titleName
        assetNLbl.text = titleNumber

    }
    
    func defaultActiveTab() {
        yOffset = 10
        print(self.AssArrkey.count)
        for i in 0..<self.AssArrkey.count {
            let label = UILabel()
            label.frame = CGRect(x: 10, y: yOffset+50, width: 120, height: 50)
            label.backgroundColor = .yellow
            yOffset = yOffset + 30 + label.frame.height
            label.textColor = UIColor.black
            let object =  self.AssFnm[i]
           
            if let fname = object["field_lable"] as? String {
                label.text = fname
                print(fname)
            }
            if object["field_type"] as? Int != 11 {
            allTabAct.addSubview(label)
               
            }
        }
        yOffset = 10
        print(self.AssArrval.count)
        for i in 0..<self.AssArrval.count{
            let label2 = UILabel()
            label2.frame = CGRect(x: 150, y: yOffset+50, width: 180, height: 50)
            label2.backgroundColor = .yellow
            yOffset = yOffset  + 30 + label2.frame.height
            label2.textColor = UIColor.black
            if let fname = AssArrval[i] as? String {
                label2.text = fname
                print(fname)
            }
            else if let fname = AssArrval[i] as? Int {
                 label2.text = String(fname)
                print(fname)
            }
            allTabAct.addSubview(label2)
        }

    }
    
    func checkListTab() {
            yOffset = 10
            for i in 0..<items.count {
            let headerLbl = UILabel()
            headerLbl.frame = CGRect(x: 10, y: yOffset+80, width: 120, height: 50)
            yOffset = yOffset + headerLbl.frame.height
            headerLbl.text = items[i]
            headerLbl.numberOfLines = 0

            allTabAct.addSubview(headerLbl)
            
            let redioBtn = UIButton()
            let redioBtn2 = UIButton()
            redioBtn.frame = CGRect(x: 10, y: yOffset+80, width: 30, height: 30)
            redioBtn2.frame = CGRect(x: 100, y: yOffset+80, width: 30, height: 30)
            let yes = UILabel()
            yes.frame = CGRect(x: 50, y: yOffset+80, width: 30, height: 30)
            yes.text = "Yes"
            
            let No = UILabel()
            No.frame = CGRect(x: 150, y: yOffset+80, width: 30, height: 30)
            No.text = "No"
            yOffset = yOffset + 50 + redioBtn.frame.height
            redioBtn.setImage(UIImage(named: "0-degrees"), for: .normal)
            redioBtn2.setImage(UIImage(named: "0-degrees"), for: .normal)

            allTabAct.addSubview(redioBtn)
            allTabAct.addSubview(yes)
            allTabAct.addSubview(redioBtn2)
            allTabAct.addSubview(No)
        }
        for i in 0..<3 {
            let headerLbl = UILabel()
            headerLbl.frame = CGRect(x: 10, y: yOffset+20, width: 120, height: 40)
            yOffset = yOffset + headerLbl.frame.height
            headerLbl.text = items[i]
            headerLbl.numberOfLines = 0
            allTabAct.addSubview(headerLbl)
            let myTextField: UITextField = UITextField(frame: CGRect(x: 10, y: yOffset+10, width: 300.00, height: 30.00));
  
           
            myTextField.placeholder = "Enter something here !"
            myTextField.borderStyle = UITextField.BorderStyle.none
            myTextField.backgroundColor = UIColor.white
            myTextField.textColor = UIColor.blue
             yOffset = yOffset  +  headerLbl.frame.height
            allTabAct.addSubview(myTextField)
            let lineLbl = UILabel()
            lineLbl.frame = CGRect(x: 10, y: yOffset, width: 320, height: 1)
            lineLbl.backgroundColor = .black
             allTabAct.addSubview(lineLbl)
        }
        let attchBtn = UIButton()
       
        attchBtn.frame = CGRect(x: 10, y: yOffset+15, width: 170, height: 40)
        image.frame = CGRect(x: 180, y: yOffset+15, width: 50, height: 50)
        allTabAct.addSubview(image)
        attchBtn.setTitle("Image Attachments", for: .normal)
        attchBtn.backgroundColor = UIColor.blue
        attchBtn.setTitleColor(.white, for: .normal)
  
        attchBtn.addTarget(self, action: #selector(self.tapAttachButton(sender:)), for: .touchUpInside)
        allTabAct.addSubview(attchBtn)
    }
    @objc func tapAttachButton(sender : UIButton){
        print("chose file")
        btnClicked()

    }
    
    @IBAction func btnClicked() {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
//        image.image = image
    }
    @objc func tappedAssetButton(sender : UIButton){
        let button = sender
        print("yes its assets")
//         allTabAct.removeFromSuperview()
        for view in self.allTabAct.subviews {
            view.removeFromSuperview()
        }
         defaultActiveTab()
//        allTabAct.reloadInputViews()
        button.backgroundColor = .blue
    }
    
    @objc func tappedButton(sender : UIButton){
        
     let button = sender
//        button.backgroundColor = UIColor(red: 17.0/255.0, green: 119.0/255.0, blue: 151.0/255.0, alpha: 1.0)
        switch sender.tag {
        case 0:
            print("button tag is :",sender.tag)
            factiveBtn = false
            for view in self.allTabAct.subviews {
                view.removeFromSuperview()
            }
            checkListTab()
//             allTabAct.reloadInputViews()
            button.backgroundColor = .blue
            break
        case 1:
            print("button tag is :",sender.tag)
            factiveBtn = false
            for view in self.allTabAct.subviews {
                view.removeFromSuperview()
            }
            button.backgroundColor = .blue
            break
        case 2:
            print("button tag is :",sender.tag)
            factiveBtn = false
            for view in self.allTabAct.subviews {
                view.removeFromSuperview()
            }
            button.backgroundColor = .blue
            break
        case 3:
            print("button tag is :",sender.tag)
            factiveBtn = false
            for view in self.allTabAct.subviews {
                view.removeFromSuperview()
            }
            button.backgroundColor = .blue
            break
        case 4:
            print("button tag is :",sender.tag)
            factiveBtn = false
            for view in self.allTabAct.subviews {
                view.removeFromSuperview()
            }
            button.backgroundColor = .blue
            break
        case 5:
            print("button tag is :",sender.tag)
            factiveBtn = false
            for view in self.allTabAct.subviews {
                view.removeFromSuperview()
            }
            button.backgroundColor = .blue
            break
        case 6:
            print("button tag is :",sender.tag)
            factiveBtn = false
            for view in self.allTabAct.subviews {
                view.removeFromSuperview()
            }
            button.backgroundColor = .blue
            break
        case 7:
             print("button tag is :",sender.tag)
            factiveBtn = false
             for view in self.allTabAct.subviews {
                view.removeFromSuperview()
             }
             button.backgroundColor = .blue
            break
        case 8:
            print("button tag is :",sender.tag)
             factiveBtn = false
            for view in self.allTabAct.subviews {
                view.removeFromSuperview()
            }
            button.backgroundColor = .blue
            break
        case 9:
            print("button tag is :",sender.tag)
            factiveBtn = false
            for view in self.allTabAct.subviews {
                view.removeFromSuperview()
            }
            break
        case 10:
            print("button tag is :",sender.tag)
            factiveBtn = false
            for view in self.allTabAct.subviews {
                view.removeFromSuperview()
            }
            break
        case 11:
            print("button tag is :",sender.tag)
            factiveBtn = false
            for view in self.allTabAct.subviews {
                view.removeFromSuperview()
            }
            break
        case 12:
            print("button tag is :",sender.tag)
            factiveBtn = false
            break
        case 13:
            print("button tag is :",sender.tag)
            factiveBtn = false
            break
        case 14:
            print("button tag is :",sender.tag)
            factiveBtn = false
            break
        case 15:
            print("button tag is :",sender.tag)
            factiveBtn = false
            break
        case 16:
            print("button tag is :",sender.tag)
            factiveBtn = false
            break
        case 17:
            print("button tag is :",sender.tag)
            factiveBtn = false
            break
        case 18:
            print("button tag is :",sender.tag)
            factiveBtn = false
            break
        case 19:
            print("button tag is :",sender.tag)
            factiveBtn = false
            break
        default:
            break
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func goBack(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}
